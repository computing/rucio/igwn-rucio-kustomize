#!/bin/bash -e
docker build --no-cache -t containers.ligo.org/rucio/igwn-rucio-kustomize/diskcache:latest . --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
