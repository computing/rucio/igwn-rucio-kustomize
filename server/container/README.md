# rucio-server

Apache server component for IGWN-rucio.

See e.g., https://github.com/rucio/containers/tree/master/server. Note:

    This container installs legacy lfn2pfn
    Uses k8s volumeMounts, not Docker volumes
    Uses k8s configMaps to mount configuration files, rather than building them into the container using the entrypoint script
    Adds osg-ca-certs package
    Only installs rucio[postgresql]
    Customizes runtime environment to pick up SSL certs for postgresql if present

We could add these on top of the upstream server image at pod startup but we adopt a minimal approach.

