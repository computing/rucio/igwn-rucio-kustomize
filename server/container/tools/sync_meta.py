#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#                       http://www.apache.org/licenses/LICENSE-2.0
#
# This version modified by James Clark for metadata in LIGO rucio deployment.
#
# Authors:
# - Vincent Garonne, <vincent.garonne@cern.ch>, 2013
# - Hannes Hansen, <hannes.jakob.hansen@cern.ch>, 2019
# - Martin Barisits, <martin.barisits@cern.ch>, 2019
# - James Alexander Clark, <james.clark@ligo.org>, 2020

import sys
import traceback

from rucio.client import Client
from rucio.common.exception import Duplicate

UNKNOWN = 3
CRITICAL = 2
WARNING = 1
OK = 0

if __name__ == '__main__':

    meta_keys = [('project', 'ALL', None, ['LIGO', 'Virgo', 'GEO600', 'KAGRA']),
                 ('run_number', 'ALL', None, []),
                 ('stream_name', 'ALL', None, []),
                 ('prod_step', 'ALL', None, ['NoProdstepDefined', 'user']),
                 ('datatype', 'ALL', None, ['GWF', 'SFT']),
                 ('version', 'ALL', None, []),
                 ('campaign', 'ALL', None, []),
                 ('guid', 'FILE', r'^(\{){0,1}[0-9a-fA-F]{8}-?[0-9a-fA-F]{4}-?[0-9a-fA-F]{4}-?[0-9a-fA-F]{4}-?[0-9a-fA-F]{12}(\}){0,1}$', []),
                 ('events', 'DERIVED', r'^\d+$', [])]

    c = Client()
    for key, key_type, value_regexp, values in meta_keys:
        try:
            try:
                c.add_key(key=key, key_type=key_type, value_regexp=value_regexp)
            except Duplicate:
                print('%(key)s already added' % locals())

            for value in values:

                try:
                    c.add_value(key=key, value=value)
                except Duplicate:
                    print('%(key)s:%(value)s already added' % locals())

                if key == 'project':
                    try:
                        c.add_scope('root', value)
                    except Duplicate:
                        print('Scope %(value)s already added' % locals())
        except:
            errno, errstr = sys.exc_info()[:2]
            trcbck = traceback.format_exc()
            print('Interrupted processing with %s %s %s.' % (errno, errstr, trcbck))

    sys.exit(OK)
