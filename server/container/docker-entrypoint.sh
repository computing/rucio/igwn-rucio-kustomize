#!/bin/bash -e

# Allow the derived images to run any additional runtime customizations
echo "Customizing runtime..."
for x in /etc/igwn/image-config.d/*.sh; do source "$x"; done

# Start apache
echo "Starting apache..."
httpd -D FOREGROUND
