#!/bin/bash -e
# Check for and copy certificates from temporary locations
test ! -z ${PGSSLROOT} || PGSSLROOT="/etc/grid-security"

if [[ ! -z ${TMP_PGSSLCERT} && -f ${TMP_PGSSLCERT} ]]; then
  export PGSSLCERT="${PGSSLROOT}/$(basename ${TMP_PGSSLCERT})"
  cp ${TMP_PGSSLCERT} ${PGSSLCERT}
  chown apache:apache ${PGSSLCERT}
  unset TMP_PGSSLCERT
fi

if [[ ! -z ${TMP_PGSSLKEY} && -f ${TMP_PGSSLKEY} ]]; then
  export PGSSLKEY="${PGSSLROOT}/$(basename ${TMP_PGSSLKEY})"
  cp ${TMP_PGSSLKEY} ${PGSSLKEY}
  chown apache:apache ${PGSSLKEY}
  unset TMP_PGSSLKEY
fi
