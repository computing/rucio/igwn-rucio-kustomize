# IGWN Rucio Deployment
Kustomization-based kubernetes deployment files for rucio in IGWN.  Contains:
 * Domain-specific containers for the rucio server, daemons and FTS authentication service
 * Application configuration files (e.g., `rucio.cfg`)
 * Kustomization bases and overlays (`kustomization.yaml`) for each service deployed

Each application or service managed by this repository is hosted in its own
directory and its deployment managed through continuous integration.

Generally, the directory hierarchy for each application follows the form:

```
application
├── base
│   ├── config
│   │   ├── templates
│   │   │   ├── config1.ini.j2
│   │   │   ├── config2.ini.j2
│   │   │   └── ...
│   │   └── values.yaml
│   ├── kustomization.yaml
│   └── resources
│       ├── deployment.yaml
│       ├── service.yaml
│       └── ...
├── container
│   ├── docker-entrypoint.sh
│   ├── Dockerfile
│   └── ...
└── overlays
    └── prod
    │   └── kustomization.yaml
    └── ...
``` 
 * `base`:
 kustomization [base](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/#bases-and-overlays)
 directory with application resources and configuration files.
 * `config`, `templates`, `values.yaml`: application configurations using jinja
   templates and centralised configuration file.
 * `resources`: manifests for each kubernetes object comprising the application or service.
 * `container`: Docker container files, including scripts for entrypoints and
   static application configurations.
 * `overlays`: kustomization
 [overlays](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/#bases-and-overlays)
 for global resource customization in each application.


