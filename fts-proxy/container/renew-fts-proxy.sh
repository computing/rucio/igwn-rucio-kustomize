#!/bin/bash -e
# james Alexander Clark <james.clark@.ligo.org>
#
# TODO: Support multiple FTS servers
# TODO: Allow additional VOMS
#
VOMS="virgo:/virgo/virgo"
PROXYHOURS=24
proxytool="/usr/bin/voms-proxy-init -voms ${VOMS} -hours ${PROXYHOURS}"
#proxytool="/usr/bin/voms-proxy-init -hours ${PROXYHOURS}"
ftsdelegate=/usr/bin/fts-rest-delegate

## Logging info
dtstamp="`date +%F-%A-%H.%M.%S `"
echo -e "\n################ ${dtstamp} ################"

## Create robot proxy
echo -e "Creating proxy"
${proxytool} -cert ${USERCERT} -key ${USERKEY} -out ${X509_USER_PROXY} -debug
echo -e "Proxy created at ${X509_USER_PROXY}"

## Delgate proxy
if [ ! -z ${FTS_SERVER} ]; then
  echo -e "Delegating proxy to ${FTS_SERVER}"
  ${ftsdelegate} -f -v -s ${FTS_SERVER} --cert=${X509_USER_PROXY} --key=${X509_USER_PROXY} --hours=${PROXYHOURS}
  echo -e "Delegation complete"
fi

## Create the corresponding kubernetes secret
echo -e "Creating k8s secret"
kubectl create secret generic proxy-cert --from-file=${X509_USER_PROXY} --dry-run=client -o yaml | kubectl apply --validate=false  -f  -
