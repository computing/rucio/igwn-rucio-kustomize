# FTS-proxy
Container for k8s cronjob to:
 * generate a proxy from the user certificate secret
 * delegate proxy to FTS
 * create a secret from this proxy for daemons to mount as volume
