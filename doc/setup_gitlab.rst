Setting Up Gitlab Integration
=============================
Add the Nautilus cluster to this gitlab instance [here](https://git.ligo.org/rucio/igwn-rucio-kustomize/clusters/new)

Set Up ServiceAccount
---------------------
Create a service account file::

  ---
  apiVersion: v1
  kind: ServiceAccount
  metadata:
    name: gitlab-admin
    namespace: ligo-rucio
  ---
  apiVersion: rbac.authorization.k8s.io/v1beta1
  kind: RoleBinding
  metadata:
    name: gitlab-admin
    namespace: ligo-rucio
  roleRef:
    apiGroup: rbac.authorization.k8s.io
    kind: ClusterRole
    name: admin
  subjects:
  - kind: ServiceAccount
    name: gitlab-admin
  namespace: ligo-rucio

Apply it::

  $  kubectl apply -f gitlab-admin-service-account.yaml

Retrive the token::

  $  k get secret $(kubectl -n ligo-rucio get secret | grep gitlab-admin | awk '{print $1}') -o yaml | grep token: | awk '{print $2}' | base64 --decode


Copy the token into the service token field in the gitlab UI

Get the ca.crt::

  $  k get secret $(kubectl -n ligo-rucio get secret | grep gitlab-admin | awk '{print $1}') -o yaml | grep ca.crt: | awk '{print $2}' | base64 --decode

Copy the `ca.crt` value into the gitlab UI



