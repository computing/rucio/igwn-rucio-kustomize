Secrets
=======
In kubernetes, certificates and passwords are mounted as volumes in running
containers using [secrets](https://kubernetes.io/docs/concepts/configuration/secret/).


